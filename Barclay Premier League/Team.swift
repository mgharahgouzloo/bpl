//
//  Team.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/18/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import Foundation

class Team {
    
    private(set) var name : String!
    private(set) var newsFeed : String!
    private(set) var videoFeed : String!
    private(set) var textColor : UIColor!
    private(set) var backgroundColor : UIColor!
    private(set) var teamColors : [UIColor]!
    
    init(teamConfig : [String:AnyObject]) {
        name = teamConfig["Name"] as? String
        
        if let tmp = teamConfig["Feed URL"] as? String {
            newsFeed = tmp
        }

        if let tmp = teamConfig["Feed URL"] as? String {
            videoFeed = tmp
        }
        
        if let tmp = teamConfig["text"] as? String {
            textColor = UIColor.colorWithHexString(tmp)
        }
        
        if let tmp = teamConfig["background"] as? String {
            backgroundColor = UIColor.colorWithHexString(tmp)
        }

        if let tmp = teamConfig["Colors"] as? [String] {
            teamColors = [UIColor]()
            for hexColor in tmp {
                teamColors.append(UIColor.colorWithHexString(hexColor))
            }
        }
    }
}