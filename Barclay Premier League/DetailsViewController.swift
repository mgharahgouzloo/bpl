//
//  DetailsViewController.swift
//  Premier League
//
//  Created by Moe Gharahgouzloo on 6/15/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, UIScrollViewDelegate, UIWebViewDelegate {

    @IBOutlet private weak var webView: UIWebView!
    @IBOutlet private weak var activityIndicator : UIActivityIndicatorView!
    @IBOutlet weak var nextBarButton: UIBarButtonItem!
    @IBOutlet weak var prevBarButton: UIBarButtonItem!

    private var teamFeeds : [RSSFeedItem]!
    private var teamName : String = ""
    private var teamColors : [UIColor]!
    private var feedIndex : Int = 0

    func prepareToSegueFor(team:Team, feeds: [RSSFeedItem], At index:Int) {
        teamFeeds = feeds
        teamName = team.name
        teamColors = team.teamColors
        feedIndex = index
        updateUI()
    }
    
    @IBAction func close(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func changeFeed(sender: UIBarButtonItem) {
        feedIndex += (sender == nextBarButton ? 1 : -1)
        updateUI()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.scrollView.delegate = self
        webView.delegate = self
        updateUI()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    private func updateUI() {
        self.navigationItem.title = self.teamName
        nextBarButton.enabled = feedIndex < teamFeeds.count - 1
        prevBarButton.enabled = feedIndex > 0

        guard webView != nil else {
            return
        }
        
        guard feedIndex >= 0 && feedIndex < teamFeeds.count else {
            print("feedindex out of range \(feedIndex) must be between 0-\(teamFeeds.count)")
            return
        }
        
        guard teamFeeds[feedIndex].link != "" else {
            print("There is no link")
            return
        }
        
        guard let url = NSURL.init(string: teamFeeds[feedIndex].link) else {
            print("Invalid URL:\(teamFeeds[feedIndex].link)")
            return
        }
        
        webView.loadRequest(NSURLRequest(URL: url))
    }
    
    // Toggle the status bar
    // - Hide the status bar when the navigation bar is hidden
    // - Show the status bar when the navigation bar is displayed
    func toggleStatusBar() {
        if let navigationBarHidden = navigationController?.navigationBarHidden {
            if navigationBarHidden && !UIApplication.sharedApplication().statusBarHidden {
                UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
            } else if !navigationBarHidden && UIApplication.sharedApplication().statusBarHidden {
                UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
            }
        }
    }
    
    // MARK: - UIScrollViewDelegate Methods
    func scrollViewDidScroll(scrollView: UIScrollView) {
        toggleStatusBar()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        activityIndicator.stopAnimating()
    }

}

