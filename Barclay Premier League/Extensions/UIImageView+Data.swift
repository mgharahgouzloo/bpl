//
//  UIImageView+Async.swift
//  RssReader
//
//  Created by Moe Gharahgouzloo on 5/28/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setImageData(imageData : NSData!, animated : Bool = true) {
        var image : UIImage!
    
        if let data = imageData {
            image = UIImage(data: data)
        }

        if animated {
            UIView.transitionWithView(self, duration: 0.5, options: .TransitionCrossDissolve, animations: {
                self.image = image
            }, completion : nil)
        }
        else {
            self.image = image
        }
    }
}
