//
//  UIColor+Extension.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/18/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import Foundation


extension UIColor
{
    class func colorWithHexString(hexString : String) -> UIColor! {
        var hex = hexString
        var rgbValue:UInt32 = 0
            
        if hex.hasPrefix("#") {
            hex = hexString.substringFromIndex(hexString.startIndex.advancedBy(1))
        }
        else if hex.hasPrefix("0x") {
            hex = hexString.substringFromIndex(hexString.startIndex.advancedBy(2))
        }

        if NSScanner(string:hex).scanHexInt(&rgbValue) {
            if hex.characters.count == 6 {
                return UIColor.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                                    blue: CGFloat( rgbValue & 0x0000FF) / 255.0,
                                    alpha: 1.0)
            }
            else if hex.characters.count == 8 {
                return UIColor.init(red:   CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0,
                                    green: CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0,
                                    blue:  CGFloat((rgbValue & 0x0000FF00) >>  8) / 255.0,
                                    alpha: CGFloat( rgbValue & 0x000000FF) / 255.0)
            }
        }

        return nil
    }
    
    func isLightColor() -> Bool {
        var white : CGFloat = 0
        
        return self.getWhite(&white, alpha: nil) && white <= 0.5
    }
    
    func isDarkColor() -> Bool {
        return !isLightColor()
    }
    
    func adjustBrightnessBy(amount : CGFloat) -> UIColor! {
        var hue : CGFloat = 0
        var saturation : CGFloat = 0
        var brightness : CGFloat = 0
        var alpha : CGFloat = 0
        
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            brightness += (amount-1.0)

            return UIColor(hue: hue, saturation: saturation, brightness: max(min(brightness, 1.0), 0.0), alpha: alpha)
        }

        var white : CGFloat = 0
        if self.getWhite(&white, alpha: &alpha) {
            white += (amount-1.0);
            return UIColor(white:max(min(white, 1.0), 0.0), alpha: alpha)
        }
        
        return nil;
    }
}
