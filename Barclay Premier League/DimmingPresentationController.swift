//
//  DimmingPresentationController.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 6/1/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class DimmingPresentationController: UIPresentationController {

    lazy var blurEffectView : UIVisualEffectView = {
        return UIVisualEffectView()
    }()
    
    override func shouldRemovePresentersView() -> Bool {
        return false
    }
    
    override func presentationTransitionWillBegin() {
//        super.presentationTransitionWillBegin()
        blurEffectView.frame = containerView!.bounds
        containerView!.insertSubview(blurEffectView, atIndex: 0)
        
        if let transitionCoordinator = presentedViewController.transitionCoordinator() {
            transitionCoordinator.animateAlongsideTransition( { _ in self.blurEffectView.effect = UIBlurEffect(style: .Dark) }, completion: nil)
        }
    }
    
    override func dismissalTransitionWillBegin() {
//        super.dismissalTransitionWillBegin()

        if let transitionCoordinator = presentedViewController.transitionCoordinator() {
            transitionCoordinator.animateAlongsideTransition({ _ in self.blurEffectView.effect = nil },
                                                  completion:{ _ in self.blurEffectView.removeFromSuperview() })
        }
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        blurEffectView.frame = containerView!.bounds
    }

}
