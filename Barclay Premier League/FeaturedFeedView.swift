//
//  FeaturedFeedView.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/28/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class FeaturedFeedView: UIView
{
    lazy var gradient : CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red: 0, green: 0, blue: 0, alpha: 0.0).CGColor,
                           UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).CGColor]
        return gradient
    }()

    @IBOutlet private weak var catagoryLabel: UILabel!
    @IBOutlet private weak var pubDateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var feedImage : UIImageView! {
        didSet {
            feedImage.layer.insertSublayer(gradient, atIndex: 0)
        }
    }

    var feed : RSSFeedItem? {
        didSet {
            updateUI()
        }
    }

    func updateUI() {
        if let currentFeed = feed {
            self.titleLabel.text = currentFeed.title
            self.catagoryLabel.text = currentFeed.categories.first
            self.pubDateLabel.text = NSDateFormatter.localizedStringFromDate(currentFeed.publicationDate, dateStyle: NSDateFormatterStyle.LongStyle, timeStyle: NSDateFormatterStyle.LongStyle)
            self.feedImage.image = nil;
            
            if currentFeed.headerImageURL == nil {
                RSSFeedService.updateFeed(currentFeed, completionHandler: { [weak weakself = self] (feed, error) in
                    let result = RSSFeedService.fetchFeedImageCached(currentFeed) { [weak weakself = self] (_, imageData, error) in
                        if currentFeed == weakself?.feed {
                            weakself?.feedImage.setImageData(imageData)
                        }
                    }
                    weakself?.feedImage.setImageData(result.cacheData)
                })
            }
            else {
                let result = RSSFeedService.fetchFeedImageCached(currentFeed) { [weak weakself = self] (_, imageData, error) in
                    if currentFeed == weakself?.feed {
                        weakself?.feedImage.setImageData(imageData)
                    }
                }
                self.feedImage.setImageData(result.cacheData)
            }
        }
        else {
            self.feedImage.image = nil
            self.titleLabel.text = " "
            self.catagoryLabel.text = " "
            self.pubDateLabel.text = " "
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = feedImage.bounds
    }
}

