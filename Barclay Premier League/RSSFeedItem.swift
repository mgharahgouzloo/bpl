//
//  RSSFeedItem.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/16/16.
//  Based on code by AppCoda on 11/20/14.
//  Copyright © 2016 RBII. All rights reserved.
//  Copyright (c) 2014 AppCoda Limited. All rights reserved.
//

import Foundation

class RSSFeedItem : NSObject {
    var link: String = ""
    var categories = [String]()
    var headerImageURL: String? = "" {
        didSet {
            headerImageURL = headerImageURL?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            headerImageURL = headerImageURL?.stringByRemovingPercentEncoding!
            headerImageURL = headerImageURL?.stringByReplacingOccurrencesOfString("&amp;", withString: "&")
        }
    }
    var authorName: String = ""
    var title: String = ""
    var publicationDate = NSDate()
    var desc: String = ""
    var rawDescription: String = ""
    var content: String? = ""
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let obj = object as? RSSFeedItem {
            return link == obj.link
        }
        return false
    }
    
    override var description : String {
        get {
            var strings = [String]()
            strings.append("Auth:\(authorName)")
            strings.append("Titl:\(title)")
            strings.append(" Pub:\(publicationDate)")
            strings.append("Desc:'\(desc)'")
            strings.append("Link:\(link)")
            strings.append(" Cat:\(categories)")
            strings.append(" IMG:\(headerImageURL)")
            return strings.joinWithSeparator("\n")
        }
    }
}