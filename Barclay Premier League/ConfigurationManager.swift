//
//  ConfigurationManager.swift
//  RssReader
//
//  Created by Simon Ng on 5/12/14.
//  Copyright (c) 2014 AppCoda Limited. All rights reserved.
//

import UIKit


class ConfigurationManager: NSObject {
    
    private struct Constants {
        static let TeamsKey = "Teams"
        static let CardViewKey = "Card View"
        
        static let ConfigurationFileName = "Configuration"
    }
    
    var configuration:AnyObject?
    lazy var teams: [Team] = ConfigurationManager.initTeams()

    override init() {
        super.init()
        
        if let configPath = NSBundle.mainBundle().pathForResource(Constants.ConfigurationFileName, ofType: "plist") {
            configuration = NSDictionary(contentsOfFile: configPath)
        }
    }
    
    class func shared() -> ConfigurationManager {
        return _shared
    }
    
    class func initTeams() -> [Team] {
        let teamsDictionary = ConfigurationManager.shared().configuration!.objectForKey(Constants.TeamsKey) as! [[String: AnyObject]]
        
        var teams = [Team]()
        for dict in teamsDictionary {
            teams.append(Team(teamConfig: dict))
        }
        return teams
    }
    
    func displayAsCards() -> Bool {
        if let b = configuration?.objectForKey(Constants.CardViewKey) as? Bool {
            return b
        }
        return false
    }
}

let _shared: ConfigurationManager = { ConfigurationManager() }()
