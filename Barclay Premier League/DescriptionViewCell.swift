//
//  DescriptionViewCell.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/21/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class DescriptionViewCell : BaseFeedViewCell {
    
    override func configure() {
        super.configure()
        stackView.spacing = 6
    }
}

