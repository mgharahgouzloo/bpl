//
//  ModalTransitionAnimator.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 6/3/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class ModalTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    let duration = 0.25
    var isPresenting = true

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return duration
    }

    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        // Set up the transform we'll use in the animation
        guard let containerView = transitionContext.containerView() else {
            return
        }
        
        let view = isPresenting ? transitionContext.viewForKey(UITransitionContextToViewKey)!
                                : transitionContext.viewForKey(UITransitionContextFromViewKey)!

        containerView.addSubview(view)

        let offScreenUp = CGAffineTransformMakeTranslation(0, -containerView.frame.height)
        let offScreenDown = CGAffineTransformMakeTranslation(0, containerView.frame.height)

        if isPresenting {
            view.transform = offScreenUp
            view.alpha = 0.5
        }
        
        UIView.animateWithDuration(duration, animations: {
            view.transform = self.isPresenting ? CGAffineTransformIdentity : offScreenDown
            view.alpha = self.isPresenting ? 1.0 : 0.5
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }

    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = true
        return self
    }

    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = false
        return self
    }
}

