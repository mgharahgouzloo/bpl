//
//  TeamSelectorViewController.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 6/1/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class TeamSelectorViewController: UIViewController {
    
    @IBOutlet weak var teamsTable: UITableView!
    private var teams = [Team]()
    private(set) var selectedTeam : Team!

    override func viewDidLoad() {
        super.viewDidLoad()

        for team in ConfigurationManager.shared().teams {
            if team.newsFeed != "" {
                teams.append(team)
            }
        }
        
        teamsTable.delegate = self
        teamsTable.dataSource = self
        teamsTable.backgroundColor = UIColor.clearColor()
        teamsTable.estimatedRowHeight = teamsTable.rowHeight
        teamsTable.rowHeight = UITableViewAutomaticDimension

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TeamSelectorViewController.close))
        gestureRecognizer.cancelsTouchesInView = false
        gestureRecognizer.delegate = self
        view.addGestureRecognizer(gestureRecognizer)
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    @IBAction func close() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension TeamSelectorViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return (touch.view === self.view)
    }
}

extension TeamSelectorViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let team = teams[indexPath.row]

        let cell = tableView.dequeueReusableCellWithIdentifier("TeamIdentifierCell", forIndexPath: indexPath)
        cell.textLabel?.text = team.name
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor.clearColor()

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedTeam = teams[indexPath.row]
        performSegueWithIdentifier("exitToHomeSegue", sender: self)
    }
}

