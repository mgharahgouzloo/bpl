//
//  FulllmageViewCell.swif
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/21/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class FulllmageViewCell: BaseFeedViewCell
{
    lazy var gradient : CAGradientLayer = {
        let gradient = CAGradientLayer()
        
        gradient.masksToBounds = true
        gradient.colors = [UIColor(red: 0, green: 0, blue: 0, alpha: 0.0).CGColor,
                           UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).CGColor,
                           UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).CGColor,
                           UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).CGColor]
        return gradient
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = scaledImageView.bounds.insetBy(dx: 0, dy: -200)
    }
    
    override func configure() {
        super.configure()
        scaledImageView.layer.insertSublayer(gradient, atIndex: 0)
    }

//    func setImageData(photo : UIImage, animated: Bool = false) {
//        let MaxItemSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width,
//                                     UIScreen.mainScreen().bounds.size.width * 0.75)
//        let photoSize = photo.size
//        let aspectRatio = photoSize.width / photoSize.height
//        var itemSize = MaxItemSize
//        
//        itemSize = CGSizeMake(MaxItemSize.width, (aspectRatio < 1 ? MaxItemSize.height * aspectRatio : MaxItemSize.height / aspectRatio))
//
//        scaledImageView.frame.size = itemSize
//        scaledImageView.image = photo
//    }
}





















