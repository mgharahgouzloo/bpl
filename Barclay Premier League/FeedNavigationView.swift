//
//  FeedNavigationView.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/28/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class FeedNavigationView: UIView
{
    @IBOutlet weak var menuButton : UIButton!
    @IBOutlet weak var titleLabel : UILabel!
}
