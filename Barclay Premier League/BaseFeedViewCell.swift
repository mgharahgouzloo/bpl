//
//  BaseFeedViewCell.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/21/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class BaseFeedViewCell : UITableViewCell {

    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var descriptionLabel: UILabel!
    @IBOutlet private(set) weak var pubDateLabel: UILabel!
    @IBOutlet private(set) weak var scaledImageView : UIImageView!
    @IBOutlet private(set) weak var stackView: UIStackView!
    
    lazy var dateFormatter : NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        dateFormatter.dateStyle = .FullStyle
        return dateFormatter;
    }()
    
    lazy private var backgroundCardView : UIView = {
        let view = UIView.init()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.whiteColor();
        view.layer.cornerRadius = 3.0
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.blackColor().colorWithAlphaComponent(0.2).CGColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.8
        
        return view
    }()

    override func awakeFromNib() {
        configure()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
    }
    
    override func prepareForReuse() {
        if scaledImageView != nil {
            scaledImageView.image = UIImage(named:"Field")
        }
    }

    func configure() {
        if ConfigurationManager.shared().displayAsCards() {
            contentView.insertSubview(backgroundCardView, atIndex: 0)
            contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
            
            backgroundCardView.topAnchor.constraintEqualToAnchor(contentView.topAnchor, constant: 3).active = true
            contentView.trailingAnchor.constraintEqualToAnchor(backgroundCardView.trailingAnchor, constant: 6).active = true
            backgroundCardView.leadingAnchor.constraintEqualToAnchor(contentView.leadingAnchor, constant: 6).active = true
            contentView.bottomAnchor.constraintEqualToAnchor(backgroundCardView.bottomAnchor, constant: 3).active = true
        }
    }
}

