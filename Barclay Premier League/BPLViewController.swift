//
//  BPLViewController.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/16/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

class BPLViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var featuredFeedHeaderView : FeaturedFeedView!
    @IBOutlet private weak var navigationView : FeedNavigationView!

    private var lastUpdate = NSDate(timeIntervalSince1970: 0)
    private var feeds = [RSSFeedItem]()

    private struct Constants {
        static let DescriptionCellIdentifier = "DescriptionViewCell"
        static let ImageCellIdentifier = "Small ImageView Cell"
        static let FullImageCellIdentifier = "Full ImageView Cell"
        
        static let ShowTeamSelectorSegueIdentifier = "ShowTeams"
        static let ShowDetailsSegueIdentifier      = "ShowDetails"
        
        static let DefaultTableViewHeaderHeight : CGFloat = 250.0
        static let DefaultNavHeaderOffset : CGFloat = 37.0
        
        static let DefaultTeam = "Default Team"
    }

    var team : Team! {
        didSet {
            updateTeamFeeds()
            
            if let index = ConfigurationManager.shared().teams.indexOf({$0.name == team.name}) {
                NSUserDefaults.standardUserDefaults().setInteger(index, forKey: Constants.DefaultTeam)
            }
            else {
                NSUserDefaults.standardUserDefaults().setInteger(0, forKey: Constants.DefaultTeam)
            }
        }
    }

    private var defaultTableHeaderHeight = Constants.DefaultTableViewHeaderHeight
    private var defaultNavHeaderOffset = Constants.DefaultNavHeaderOffset
    private var isRefreshingContent = false

    lazy private var newsService : RSSFeedService = {
       let service = RSSFeedService()
        service.delegate = self
        service.dataSource = self
        return service
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.separatorStyle = ConfigurationManager.shared().displayAsCards() ? .None : .SingleLine
        
        // Add the header view to the table view background
        defaultTableHeaderHeight = featuredFeedHeaderView.frame.size.height
        featuredFeedHeaderView.feed = nil
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BPLViewController.featuredFeedHandleTap))
        featuredFeedHeaderView.titleLabel.addGestureRecognizer(tapGestureRecognizer)
        featuredFeedHeaderView.titleLabel.userInteractionEnabled = true
        
        featuredFeedHeaderView.feedImage.addGestureRecognizer(tapGestureRecognizer)
        featuredFeedHeaderView.feedImage.userInteractionEnabled = true
        
        tableView.tableHeaderView = nil
        tableView.addSubview(featuredFeedHeaderView)
        tableView.sendSubviewToBack(featuredFeedHeaderView)
        
        tableView.contentInset = UIEdgeInsets(top: defaultTableHeaderHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -defaultTableHeaderHeight)
        
        if traitCollection.forceTouchCapability == .Available {
            registerForPreviewingWithDelegate(self, sourceView: tableView)
        }


        team = ConfigurationManager.shared().teams[NSUserDefaults.standardUserDefaults().integerForKey(Constants.DefaultTeam)]
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    func reloadTableView(newsFeeds: [RSSFeedItem]) {

        var indexPathsToDelete = [NSIndexPath]()
        for index in 0 ..< feeds.count {
            indexPathsToDelete.append(NSIndexPath(forRow: index, inSection: 0))
        }
        
        feeds = newsFeeds

        var indexPathsToInsert = [NSIndexPath]()
        for index in 0 ..< feeds.count {
            feeds[index].headerImageURL = nil
            indexPathsToInsert.append(NSIndexPath(forRow: index, inSection: 0))
        }
        
        updateHeaderView()

        tableView.beginUpdates()
        tableView.deleteRowsAtIndexPaths(indexPathsToDelete, withRowAnimation: .None)
        tableView.insertRowsAtIndexPaths(indexPathsToInsert, withRowAnimation: .None)
        tableView.endUpdates()
        
        self.featuredFeedHeaderView.feed = self.feeds.first
        self.tableView.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated:false)

        navigationView.titleLabel.text = team.name
    }

    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        // Update the bounds of the blur effect view when the orientation change
    }

    override func viewDidLayoutSubviews() {
        updateHeaderView()
    }
    
    func updateHeaderView() {
        let headerViewFrame = CGRect(x: 0, y: -defaultTableHeaderHeight, width: tableView.bounds.size.width, height: defaultTableHeaderHeight)
        featuredFeedHeaderView.frame = headerViewFrame
    }
    
    func featuredFeedHandleTap(gesture: UIGestureRecognizer) {
        self.performSegueWithIdentifier(Constants.ShowDetailsSegueIdentifier, sender: nil)
    }
    
    func updateTeamFeeds() {
        newsService.cancelFetch()
        
        if team != nil {
            self.tableView.userInteractionEnabled = false
            newsService.fetchFeeds();
        }
    }
    
    let modalAnimationTransition = ModalTransitionAnimator()
    let popAnimationTransition = PopTransitionAnimator()

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.ShowTeamSelectorSegueIdentifier {
            segue.destinationViewController.transitioningDelegate = modalAnimationTransition
        }
        else if segue.identifier == Constants.ShowDetailsSegueIdentifier {
            if let dvc =  segue.destinationViewController.contentViewController as? DetailsViewController {
                dvc.prepareToSegueFor(team, feeds: feeds, At: sender == nil ? 0 : tableView.indexPathForSelectedRow!.row)
                segue.destinationViewController.transitioningDelegate = popAnimationTransition
            }
        }
    }
    
    @IBAction func exitToHome(segue: UIStoryboardSegue) {
        if let teamSelectorController = segue.sourceViewController as? TeamSelectorViewController {
            team = teamSelectorController.selectedTeam
        }
    }
}

extension BPLViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        // Stretchy header
        var headerViewFrame = CGRect(x: 0, y: -defaultTableHeaderHeight,
                                     width: tableView.bounds.size.width, height: defaultTableHeaderHeight)
        
        // Keep the view origin to the top
        if scrollView.contentOffset.y < -defaultTableHeaderHeight {
            headerViewFrame.origin.y = scrollView.contentOffset.y
            headerViewFrame.size.height =  -scrollView.contentOffset.y
        }
        
        featuredFeedHeaderView.frame = headerViewFrame
        
        // Change the background color of the navigation header as the user scrolls up
        let offsetY = scrollView.contentOffset.y + defaultTableHeaderHeight
        
        if offsetY < defaultNavHeaderOffset {
            navigationView.backgroundColor = UIColor.clearColor()
        } else if offsetY > defaultTableHeaderHeight {
            navigationView.backgroundColor = team.teamColors?.count > 0 ? team.teamColors?.first : UIColor.blackColor()

        } else {
            let color = team.teamColors?.count > 0 ? team.teamColors?.first : UIColor.blackColor()
            navigationView.backgroundColor = color?.colorWithAlphaComponent((offsetY - defaultNavHeaderOffset) / (defaultTableHeaderHeight - defaultNavHeaderOffset))
        }
        
        // Pull to refresh
        if !isRefreshingContent {
            if -offsetY > 60 {
//                blurringView.hidden = false
//                blurringView.alpha = (-offsetY - 60) / 40
                
            } else {
//                blurringView.hidden = true
//                blurringView.alpha = 0.0
            }
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y + defaultTableHeaderHeight
        if -offsetY > 100 {
            isRefreshingContent = true
            updateTeamFeeds()
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        // Update the size of the header image
        coordinator.animateAlongsideTransition({ (context) -> Void in
            self.updateHeaderView()
            }, completion: {(context) -> Void in
        })
    }
}

extension BPLViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            return UITableViewCell(frame: CGRectZero)
        }

        let cellIdentifier = indexPath.row % 5 == 0 ? Constants.DescriptionCellIdentifier : Constants.ImageCellIdentifier
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! BaseFeedViewCell
        
        let feed = feeds[indexPath.row]

        cell.titleLabel?.text = feed.title
        cell.descriptionLabel?.text = feed.desc
        cell.pubDateLabel?.text = cell.dateFormatter.stringFromDate(feed.publicationDate)

        // Load article image
        if feed.headerImageURL == nil {
            RSSFeedService.updateFeed(feed) { (currentFeed, error) in
                if error == nil {
                    let result = RSSFeedService.fetchFeedImageCached(feed) { (_, imageData, error) in
                        if imageData != nil {
                            cell.scaledImageView?.setImageData(imageData)
                        }
                    }
                    
                    if result.cacheData != nil {
                        cell.scaledImageView?.setImageData(result.cacheData, animated: false)
                    }
                    cell.descriptionLabel?.text = currentFeed.desc
                }
            }
        }
        else {
            let result = RSSFeedService.fetchFeedImageCached(feed) { (_, imageData, error) in
                if let imageData = imageData {
                    cell.scaledImageView?.setImageData(imageData)
                }
            }
            cell.scaledImageView?.setImageData(result.cacheData, animated: false)
        }
    
        return cell;
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 0 ? 0.0 : UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier(Constants.ShowDetailsSegueIdentifier, sender: self)
    }
}

extension BPLViewController : RSSFeedServiceDelegate, RSSFeedServiceDataSource {
    
    var rssFeedServiceURL : String {
        return (team?.newsFeed!)!
    }
    
    func rssFeedServiceDidStart(service : RSSFeedService) {
        isRefreshingContent = true
        lastUpdate = NSDate()
    }

    func rssFeedServiceDidComplete(service : RSSFeedService, feeds: [RSSFeedItem]!, error: NSError!) {
        isRefreshingContent = false
        tableView.userInteractionEnabled = true

        if let error = error {
            // Display alert
            let alertController = UIAlertController(title: error.localizedDescription, message: error.localizedFailureReason, preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else {
            reloadTableView(service.feeds)
        }
    }
}

extension BPLViewController : UIViewControllerPreviewingDelegate
{
    // peek
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController?
    {
        var indexPath = tableView.indexPathForRowAtPoint(location);
        
        if indexPath == nil {
            indexPath = NSIndexPath(forItem: 0, inSection: 0)
        }

        guard let cell = tableView.cellForRowAtIndexPath(indexPath!)  else {
            return nil
        }
        
        if let detailVC = storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as? DetailsViewController {
            previewingContext.sourceRect = cell.frame
            
            let feedItem = RSSFeedItem()
            feedItem.link = "http://www.readability.com/m?url=" + feeds[indexPath!.row].link
            detailVC.prepareToSegueFor(team, feeds: [ feedItem ], At: 0)

            return detailVC
        }
        
        return nil
    }
    
    // pop
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController)
    {
    }
}


private extension UIViewController {
    var contentViewController : UIViewController {
        get {
            if let nvc = self as? UINavigationController {
                return nvc.visibleViewController ??  self
            }
            return self;
        }
    }
}

