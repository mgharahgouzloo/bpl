//
//  RBIFeedLoadingView.swift
//  EPLProgressAnimationView
//
//  Created by Moe Gharahgouzloo on 5/22/16.
//  Copyright © 2016 RBII. All rights reserved.
//

import UIKit

@IBDesignable class FeedProgressIndicator : UIView {

    private var replicatorLayer : CAReplicatorLayer!
    
    private func setupLoadingAnimations() {
        let size = self.bounds.size
        let dotNum: CGFloat = 10
        let diameter: CGFloat = size.width / 10
        
        let dot = CALayer()
        let frame = CGRect(
            x: (layer.bounds.width - diameter) / 2 + diameter * 2,
            y: (layer.bounds.height - diameter) / 2,
            width: diameter,
            height: diameter
        )
        
        dot.backgroundColor = tintColor.CGColor
        dot.cornerRadius = diameter / 2
        dot.frame = frame
        
        replicatorLayer = CAReplicatorLayer()
        replicatorLayer.frame = layer.bounds
        replicatorLayer.instanceCount = Int(dotNum)
        replicatorLayer.instanceDelay = 0.1
        
        let angle = (2.0 * M_PI) / Double(replicatorLayer.instanceCount)
        
        replicatorLayer.instanceTransform = CATransform3DMakeRotation(CGFloat(angle), 0.0, 0.0, 1.0)
        
        layer.addSublayer(replicatorLayer)
        
        replicatorLayer.addSublayer(dot)
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.toValue = 0.4
        scaleAnimation.duration = 0.5
        scaleAnimation.autoreverses = true
        scaleAnimation.repeatCount = .infinity
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        dot.addAnimation(scaleAnimation, forKey: "scaleAnimation")
        
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.toValue = -2.0 * M_PI
        rotationAnimation.duration = 6.0
        rotationAnimation.repeatCount = .infinity
        rotationAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        replicatorLayer.addAnimation(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func startAnimation()
    {
        replicatorLayer?.removeFromSuperlayer()
    }
    
    func stopAnimation()
    {
        setupLoadingAnimations()
    }
    
    
}