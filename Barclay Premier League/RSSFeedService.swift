//
//  RSSFeedService.swift
//  Barclay Premier League
//
//  Created by Moe Gharahgouzloo on 5/16/16.
//  Copyright © 2016 RBII. All rights reserved.
//
import Foundation

@objc protocol RSSFeedServiceDelegate : NSObjectProtocol {
    optional func rssFeedServiceDidStart(service : RSSFeedService)
    optional func rssFeedServiceProgressed(service : RSSFeedService, progress : NSProgress)
    optional func rssFeedService(service : RSSFeedService, didParseFeedItem item : RSSFeedItem)
    
    func rssFeedServiceDidComplete(service : RSSFeedService, feeds: [RSSFeedItem]!, error: NSError!)
}

@objc protocol RSSFeedServiceDataSource : NSObjectProtocol {
    var rssFeedServiceURL : String { get }
    optional func rssFeedServiceShouldKeepParsing(service : RSSFeedService) -> Bool
}

class RSSFeedService : NSObject {

    private enum RSSFeedType: String {
        case Unknown = "unknown"
        case Atom = "feed"
        case RSS1 = "rdf:RDF"
        case RSS1Alt = "RDF"
        case RSS2 = "rss"
        
        func feedDateFormat() -> String {
            if self == .Atom {
                return "yyyy-MM-dd'T'HH:mm:ssZ"
            } else if self == .RSS1 || self == .RSS1Alt || self == .RSS2 {
                return "EEE, dd MMM yyyy HH:mm:ss ZZZ"
            }
            
            return ""
        }
    }

    private struct  Rss {
        static let ChannelTag       = "channel"
        static let ItemTag          = "item"
        static let TitleTag         = "title"
        static let LinkTag          = "link"
        static let AuthorTag        = "dc:creator"
        static let CategoryTag      = "category"
        static let ThumbnailTag     = "media:thumbnail"
        static let MediaContentTag  = "media:content"
        static let CommentsCountTag = "slash:comments"
        static let PubDateTag       = "pubDate"
        static let DescriptionTag   = "description"
        static let ContentTag       = "content:encoded"
        static let EnclosureTag     = "enclosure"
        static let GuidTag          = "guid"
    }

    private struct Atom {
        static let ChannelTag      = "feed"
        static let ItemTag         = "entry"
        static let TitleTag        = "title"
        static let LinkTag         = "link"
        static let AuthorTag       = "author"
        static let AuthorNameTag   = "name"
        static let CategoryTag     = "category"
        static let ThumbnailTag    = "media:thumbnail"
        static let MediaContentTag = "media:content"
        static let CommentsCountTag = "slash:comments"
        static let PubDateTag      = "published"
        static let ContentTag      = "content"
    }
    
    private struct Meta {
        static let Name             = "name"
        static let Property         = "property"
        static let Content          = "content"
        
        static let Author           = "author"
        static let Keywords         = "keywords"
        
        static let Image            = "og:image"
        static let Title            = "og:title"
        static let OGDescription    = "og:description"
        static let Description      = "description"
        
        static let Query            = "/html/head/meta"
    }

    private var currentElement = ""
    private var title = ""
    private var descriptionImageURL = ""
    private var thumbnailImageURL = ""
    private var mediaContentImageURL = ""
    private var enclosureImageURL = ""
    private var contentImageURL = ""
    private var feedType:RSSFeedType?
    private var currentFeed: RSSFeedItem?
    private var isParsingItem = false
    private var isParsingAuthor = false
    private var publicationDate = ""

    private var parser: NSXMLParser!
    private var fetchTask : NSURLSessionDataTask!

    private(set) var fetchURL : String!
    private(set) var feeds = [RSSFeedItem]()
    private(set) var fetchProgress : NSProgress? = nil {
        didSet {
            if let progress = fetchProgress {
                delegate?.rssFeedServiceProgressed?(self, progress: progress)
            }
        }
    }
    
    weak var dataSource : RSSFeedServiceDataSource!
    weak var delegate : RSSFeedServiceDelegate!

    func fetchFeeds() {
        guard dataSource != nil else {
            return
        }
        
        fetchURL = dataSource.rssFeedServiceURL
        
        let currentFetchURL = fetchURL
        let manager = AFHTTPSessionManager()
        
        manager.requestSerializer.setValue("text/html", forHTTPHeaderField: "Content-Type")
        manager.responseSerializer = AFHTTPResponseSerializer()

        self.delegate?.rssFeedServiceDidStart?(self)
        fetchTask = manager.GET(fetchURL,
                                parameters: nil,
                                progress: { [weak weakself = self] (progress) in
                                    dispatch_async(dispatch_get_main_queue()) {
                                        if currentFetchURL == weakself?.fetchURL {
                                            weakself?.fetchProgress = progress
                                        }
                                    }
                                },
                                success: { [weak weakself = self] (dataTask : NSURLSessionDataTask, response:AnyObject?) in
                                    dispatch_async(dispatch_get_main_queue()) {
                                        if currentFetchURL == weakself?.fetchURL {
                                            weakself?.parser = NSXMLParser(data: response as! NSData)
                                            weakself?.parser.delegate = self
                                            weakself?.parser?.shouldResolveExternalEntities = true
                                            weakself?.parser.parse()
                                        }
                                    }
                                },
                                failure: { [weak weakself = self] (_, error : NSError) in
                                    
                                    dispatch_async(dispatch_get_main_queue()) {
                                        if currentFetchURL == weakself?.fetchURL {
                                            weakself?.delegate?.rssFeedServiceDidComplete(weakself!, feeds: nil, error: error)
                                        }
                                    }
                                })
    }

    class func updateFeed(feed : RSSFeedItem, completionHandler:(feed : RSSFeedItem, error : NSError!) -> Void) -> Bool {
        
        guard let feedURL = NSURL(string:feed.link) else {
            return false;
        }
        
        let session = NSURLSession.init(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        let request = NSURLRequest.init(URL: feedURL)
        session.dataTaskWithRequest(request) { (htmlData, response, error) in
            dispatch_async(dispatch_get_main_queue()) {
                if let htmlData = htmlData, response = response as? NSHTTPURLResponse where response.statusCode == 200 {
                    feed.content = String.init(data: htmlData, encoding: NSUTF16StringEncoding)!
                    
                    let parser = TFHpple(HTMLData: htmlData)
                    for node in parser.searchWithXPathQuery(Meta.Query) {
                        if let object = node.objectForKey(Meta.Name) {
                            let name = object as! String
                            
                            if name == Meta.Author && feed.authorName == "" {
                                feed.authorName = node.objectForKey(Meta.Content) as String
                            }
                            else if name == Meta.Keywords && feed.categories.count == 0 {
                                feed.categories = ((node.objectForKey(Meta.Content) as? String)?.componentsSeparatedByString(","))!
                            }
                            else if name == Meta.Image && feed.headerImageURL == nil {
                                feed.headerImageURL = node.objectForKey(Meta.Content) as? String
                            }
                            else if name == Meta.Description && node.objectForKey(Meta.Content) != nil {
                                feed.desc = node.objectForKey(Meta.Content)
                            }
                        }
                        else if let property = node.objectForKey(Meta.Property) {
                            let name = property as! String
                            if name == Meta.Image && (feed.headerImageURL == nil || feed.headerImageURL?.characters.count ==  0) {
                                feed.headerImageURL = node.objectForKey(Meta.Content) as? String
                            }
                            // updating the title and desc really messes up the ui on updates specially with Norwich. 
                            // Their titles are very different on the page than the feed. 
                            else if name == Meta.Title && node.objectForKey(Meta.Content) != nil && feed.title == "" {
                                feed.title =  node.objectForKey(Meta.Content)
                            }
                            else if (name == Meta.Description || name == Meta.OGDescription) && node.objectForKey(Meta.Content) != nil && feed.desc == "" {
                                feed.desc =  node.objectForKey(Meta.Content)
                            }
                        }
                    }
                }
                completionHandler(feed: feed, error: error)
            }
        }.resume()
        
        return true
    }

    class func fetchFeedImageCached(feed : RSSFeedItem, completionHandler:(feed : RSSFeedItem, imageData : NSData!, error : NSError!) -> Void) -> (fetchPending: Bool, cacheData: NSData!) {
        
        guard let headerImageURL = feed.headerImageURL else {
            return (false, nil)
        }
        
        guard let imageURL = NSURL(string:headerImageURL) else {
            return (false, nil)
        }
        
        if let imageData = CacheManager.sharedCacheManager().cache.objectForKey(headerImageURL) as? NSData {
            return (true, imageData)
        }
        
        let session = NSURLSession.init(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        let request = NSURLRequest.init(URL: imageURL)
        
        session.dataTaskWithRequest(request) { (imageData, response, error) in
            dispatch_async(dispatch_get_main_queue()) {
                if let imageData = imageData, response = response as? NSHTTPURLResponse where response.statusCode == 200 {
                    CacheManager.sharedCacheManager().cache.setObject(imageData, forKey: headerImageURL)
                }
                completionHandler(feed: feed, imageData: imageData, error: error)
            }
        }.resume()
        
        return (true, nil)
    }
    
    func cancelFetch() {
        if fetchTask != nil {
            fetchTask.cancel()
            fetchTask = nil
        }
        fetchProgress = nil
    }
}

extension RSSFeedService : NSXMLParserDelegate {
    func parserDidStartDocument(parser: NSXMLParser) {
        guard fetchTask != nil else {
            return;
        }
        self.feeds = [RSSFeedItem]()
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        guard fetchTask != nil else {
            return;
        }

        currentElement = elementName

        // Determine the feed type
        switch elementName {
        case RSSFeedType.Atom.rawValue:
            self.feedType = .Atom
            return
            
        case RSSFeedType.RSS1.rawValue:
            self.feedType = .RSS1
            return
            
        case RSSFeedType.RSS1Alt.rawValue:
            self.feedType = .RSS1
            return
            
        case RSSFeedType.RSS2.rawValue:
            self.feedType = .RSS2
            return
            
        default:
            break
        }
        
        // Perform parsing based on the feed type
        if self.feedType == .RSS2 || self.feedType == .RSS1 || self.feedType == .RSS1Alt {
            parseRSSStartElement(elementName, attributes: attributeDict)
        } else if self.feedType == .Atom {
            parseAtomStartElement(elementName, attributes: attributeDict)
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String)
    {
        guard fetchTask != nil else {
            return;
        }

        // Perform parsing based on the feed type
        if self.feedType == .RSS2 || self.feedType == .RSS1 || self.feedType == .RSS1Alt {
            parseRSSFoundCharacters(foundCharacters: string)
        } else if self.feedType == .Atom {
            parseAtomFoundCharacters(foundCharacters: string)
        }
    }

    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        guard fetchTask != nil else {
            return;
        }

        // Perform parsing based on the feed type
        if self.feedType == .RSS2 || self.feedType == .RSS1 || self.feedType == .RSS1Alt {
            parseRSSEndElement(elementName)
        } else if self.feedType == .Atom {
            parseAtomEndElement(elementName)
        }
    }

    
    func parserDidEndDocument(parser: NSXMLParser)
    {
        guard fetchTask != nil else {
            return;
        }

        self.delegate?.rssFeedServiceDidComplete(self, feeds:feeds, error: nil)
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError)
    {
        guard fetchTask != nil else {
            return;
        }

        self.delegate?.rssFeedServiceDidComplete(self, feeds:nil, error: parseError)
    }

    func parseRSSStartElement(elementName: String, attributes attributeDict: [NSObject : AnyObject])
    {
        if currentElement == Rss.ItemTag {
            currentFeed = RSSFeedItem();
            descriptionImageURL = ""
            thumbnailImageURL = ""
            mediaContentImageURL = ""
            enclosureImageURL = ""
        }
        
        if currentElement == Rss.ThumbnailTag {
            if var url = attributeDict["url"] as? String {
                url = url.componentsSeparatedByString("?")[0]
                currentFeed?.headerImageURL = url
                thumbnailImageURL = url
            }
        }
        
        if currentElement == Rss.MediaContentTag {
            if var url = attributeDict["url"] as? String {
                url = url.componentsSeparatedByString("?")[0]
                if mediaContentImageURL == "" || mediaContentImageURL.contains("gravatar.com") {
                    currentFeed?.headerImageURL = url
                    mediaContentImageURL = url
                }
            }
        }
        
        if currentElement == Rss.EnclosureTag {
            if var url = attributeDict["url"] as? String {
                url = url.componentsSeparatedByString("?")[0]
                currentFeed?.headerImageURL = url
                enclosureImageURL = url
            } else if let type = attributeDict["type"] as? String {
                if type.contains("video") && type.contains("audio") {
                    var url = attributeDict["url"] as! String
                    url = url.componentsSeparatedByString("?")[0]
                    currentFeed?.headerImageURL = url
                    enclosureImageURL = url
                }
            }
        }
    }
    
    func parseAtomStartElement(elementName: String, attributes attributeDict: [NSObject : AnyObject])
    {
        if currentElement == Atom.ItemTag {
            currentFeed = RSSFeedItem();
            descriptionImageURL = ""
            thumbnailImageURL = ""
            mediaContentImageURL = ""
            isParsingItem = true
        }
        
        if isParsingItem {
            if currentElement == Atom.LinkTag {
                let url = attributeDict["href"] as! String
                currentFeed?.link = url
            }
            
            if currentElement == Atom.AuthorTag {
                isParsingAuthor = true
            }
        }
    }

    func parseRSSEndElement(elementName: String)
    {
        if elementName == Rss.CommentsCountTag {
            // skip
        }
        else if elementName == Rss.ItemTag {
            currentFeed?.title = (currentFeed?.title.stringByConvertingHTMLToPlainText())!
            if let headerImageURL = currentFeed?.headerImageURL {
                if headerImageURL == "" {
                    if thumbnailImageURL != "" {
                        currentFeed?.headerImageURL = thumbnailImageURL
                    } else if mediaContentImageURL != "" {
                        currentFeed?.headerImageURL = mediaContentImageURL
                    } else if enclosureImageURL != "" {
                        currentFeed?.headerImageURL = enclosureImageURL
                    } else if descriptionImageURL != "" {
                        currentFeed?.headerImageURL = descriptionImageURL
                    } else if contentImageURL != "" {
                        currentFeed?.headerImageURL = contentImageURL
                    }
                }
            }
            feeds.append(currentFeed!)
            delegate.rssFeedService?(self, didParseFeedItem: currentFeed!)
        }
        else if elementName == Rss.AuthorTag {
            if let _str = currentFeed?.authorName.stringByConvertingHTMLToPlainText() {
                currentFeed?.authorName = _str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
        }
        else if elementName == Rss.PubDateTag {
            let pubDate = publicationDate.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = feedType?.feedDateFormat()
            if let pubDate = dateFormatter.dateFromString(pubDate) {
                currentFeed?.publicationDate = pubDate
                publicationDate = ""
            }
        }
        else if currentElement == Rss.DescriptionTag {
            currentFeed?.desc = (currentFeed?.rawDescription.stringByDecodingHTMLEscapeCharacters())!
            if let headerImageURL = currentFeed?.headerImageURL {
                if !isImage(headerImageURL) {
                    if let html = currentFeed?.rawDescription {
                        currentFeed?.headerImageURL = html.parseFirstImage()
                        descriptionImageURL = html.parseFirstImage()!
                    }
                }
            }
        }
        else if currentElement == Rss.ContentTag {
            if let headerImageURL = currentFeed?.headerImageURL {
                if !isImage(headerImageURL) {
                    if let html = currentFeed?.content {
                        currentFeed?.headerImageURL = html.parseFirstImage()
                        contentImageURL = html.parseFirstImage()!
                    }
                }
            }
            
        }
        
    }
    
    func parseAtomEndElement(elementName: String)
    {
        if elementName == Atom.ItemTag {
            currentFeed?.title = (currentFeed?.title.stringByConvertingHTMLToPlainText())!
            feeds.append(currentFeed!)
            if let headerImageURL = currentFeed?.headerImageURL {
                if headerImageURL == "" {
                    if thumbnailImageURL != "" {
                        currentFeed?.headerImageURL = thumbnailImageURL
                    } else if mediaContentImageURL != "" {
                        currentFeed?.headerImageURL = mediaContentImageURL
                    } else if enclosureImageURL != "" {
                        currentFeed?.headerImageURL = enclosureImageURL
                    } else if descriptionImageURL != "" {
                        currentFeed?.headerImageURL = descriptionImageURL
                    }
                }
            }
            
            isParsingItem = false
            isParsingAuthor = false
        }
        else if isParsingAuthor && elementName == Atom.AuthorNameTag {
            if let author = currentFeed?.authorName.stringByConvertingHTMLToPlainText() {
                currentFeed?.authorName = author
            }
        }
        else if elementName == Atom.PubDateTag {
            let pubDate = publicationDate.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = feedType?.feedDateFormat()
            if let pubDate = dateFormatter.dateFromString(pubDate) {
                currentFeed?.publicationDate = pubDate
                publicationDate = ""
            }
        }
        else if currentElement == Atom.ContentTag {
            if let headerImageURL = currentFeed?.headerImageURL {
                if !isImage(headerImageURL) {
                    if let html = currentFeed?.rawDescription {
                        currentFeed?.headerImageURL = html.parseFirstImage()
                        descriptionImageURL = html.parseFirstImage()!
                    }
                }
            }
        }
    }
    
    func parseRSSFoundCharacters(foundCharacters string: String?)
    {
        if let currentString = string {
            switch currentElement {
            case Rss.TitleTag :
                currentFeed?.title += currentString
            case Rss.AuthorTag:
                currentFeed?.authorName += currentString
            case Rss.CategoryTag:
                let category = currentString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                if category != "" {
                    currentFeed?.categories.append(category)
                }
            case Rss.LinkTag:
                currentFeed?.link += currentString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            case Rss.PubDateTag:
                publicationDate += currentString
            case Rss.DescriptionTag:
                currentFeed?.rawDescription += currentString
            case Rss.ContentTag:
                currentFeed!.content? += currentString
            default:
                break;
            }
        }
    }
    
    func parseAtomFoundCharacters(foundCharacters string: String?)
    {
        if !isParsingItem {
            return
        }
        
        if let currentString = string?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) {
            switch currentElement {
            case Atom.TitleTag :
                currentFeed!.title += currentString
            case Atom.AuthorNameTag:
                currentFeed!.authorName += currentString
            case Atom.CategoryTag:
                let category = currentString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                if category != "" {
                    currentFeed?.categories.append(category)
                }
            case Atom.LinkTag:
                currentFeed?.link += currentString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            case Atom.PubDateTag:
                publicationDate += currentString
            case Atom.ContentTag:
                currentFeed?.rawDescription += currentString
            default:
                break;
            }
        }
    }

    func isImage(imagePath:String) -> Bool
    {
        if imagePath == "" ||
            imagePath.rangeOfString(".png", options: .CaseInsensitiveSearch) == nil ||
            imagePath.rangeOfString(".jpg", options: .CaseInsensitiveSearch) == nil ||
            imagePath.rangeOfString(".jpeg", options: .CaseInsensitiveSearch) == nil ||
            imagePath.rangeOfString(".gif", options: .CaseInsensitiveSearch) == nil {
            
            return false
        }
        
        return true
    }
}

